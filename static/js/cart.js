
$(document).ready(function(){
    var updateBtns = $(".update-cart")

    updateBtns.on('click', function() {
        var productId = $(this).attr('data-product')
        var action = $(this).attr('data-action')
        console.log('productID:', productId, 'Action:', action)

        console.log('user:', user)
        if(user === 'AnonymousUser'){
            console.log('Not logged in')
        }else{
            updateUserOrder(productId, action)
        }
    });
})


function updateUserOrder(productId, action){
    console.log('User is logged in, sending data..')

    var url = '/update_item/'

    fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': csrftoken,
        },
        body: JSON.stringify({'productId': productId, 'action': action})
    })
     .then((response) => {
        return response.json()
    })
    .then((data) => {
        console.log('data:', data)
        location.reload()
    })
}
