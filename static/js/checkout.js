if(shipping == 'False'){
    $('#shipping-info').empty()
}

if(user != 'AnonymousUser'){
    $('#user-info').empty()
}

if(shipping == 'False' && user != 'AnonymousUser'){
    //Hide entire form if user is logged in and shipping is false
    $('#form-wrapper').addClass('hidden')
    //Show payment if logged in user wants to buy an item that does not require shipping
    $('#payment-info').removeClass('hidden')
}


var form = $('form')
//console.log(form.address.value)
form.submit(function(e){
    e.preventDefault()
    console.log('Form submitted')
    $('#form-button').addClass('hidden')
    $('#payment-info').removeClass('hidden')
})


$("#make-payment").click(function(e){
    submitFormData()
})

function submitFormData(){
    console.log('Payment button clicked')
    var userFormData = {
        'name': null,
        'email': null,
        'total': total,
    }

    var shippingInfo = {
        'address': null,
        'city': null,
        'state': null,
        'zipcode': null,
    }

    if(shipping != 'False'){
        shippingInfo.address =  $('form input[name=address]').val()
        shippingInfo.city = $('form input[name=city]').val()
        shippingInfo.state = $('form input[name=state]').val()
        shippingInfo.zipcode = $('form input[name=zipcode]').val()
    }

    if(user == 'AnonymousUser'){
        userFormData.name = $('form input[name=name]').val()
        userFormData.email = $('form input[name=email]').val()
    }

    console.log('Shipping Info:', shippingInfo)
    console.log('User Info:', userFormData)

    var url = '/process_order/'
    fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': csrftoken,
        },
        body: JSON.stringify({'shipping': shippingInfo, 'form': userFormData})
    })
     .then((response) => {
        return response.json()
    })
    .then((data) => {
        console.log('Success:', data)
        alert('Transaction is completed')
        window.location = base_url
    })

}